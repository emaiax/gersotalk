## :pager: GersoTalk Communicator

Requirements:

```
JDK 1.8
Kotlin 1.3.10
```

[Multirun](https://plugins.jetbrains.com/plugin/7248-multirun) plugin needed to run the whole stack.