package main

import org.junit.Test
import kotlin.test.*

class MessageParserTest {
    @Test
    fun quitMessageEmpty() {
        val message = MessageParser("/quit").parsedMessage()

        assertEquals(MessageKind.QUIT, message.kind)
        assertEquals("SERVER", message.recipient)
        assertEquals("", message.message)

        assertTrue(message.isValid())
    }

    @Test
    fun quitMessageFilled() {
        val message = MessageParser("/quit cyah fellows").parsedMessage()

        assertEquals(MessageKind.QUIT, message.kind)
        assertEquals("SERVER", message.recipient)
        assertEquals("cyah fellows", message.message)

        assertTrue(message.isValid())
    }

    @Test
    fun quitMessageMentioning() {
        val message = MessageParser("/quit @emaiax ciao, dude").parsedMessage()

        assertEquals(MessageKind.QUIT, message.kind)
        assertEquals("SERVER", message.recipient)
        assertEquals("@emaiax ciao, dude", message.message)

        assertTrue(message.isValid())
    }

    @Test
    fun privateMessageEmpty() {
        val message = MessageParser("/pvt").parsedMessage()

        assertEquals(MessageKind.PRIVATE_MESSAGE, message.kind)
        assertEquals("SERVER", message.recipient)
        assertEquals("", message.message)

        assertFalse(message.isValid())
    }

    @Test
    fun privateMessageFilled() {
        val message = MessageParser("/pvt wtf dude").parsedMessage()

        assertEquals(MessageKind.PRIVATE_MESSAGE, message.kind)
        assertEquals("SERVER", message.recipient)
        assertEquals("wtf dude", message.message)

        assertTrue(message.isValid())
    }

    @Test
    fun privateMessageMentioning() {
        val message = MessageParser("/pvt @emaiax talk to me").parsedMessage()

        assertEquals(MessageKind.PRIVATE_MESSAGE, message.kind)
        assertEquals("@emaiax", message.recipient)
        assertEquals("talk to me", message.message)

        assertTrue(message.isValid())
    }

    @Test
    fun fileMessageEmpty() {
        val message = MessageParser("/file    ").parsedMessage()

        assertEquals(MessageKind.FILE, message.kind)
        assertEquals("SERVER", message.recipient)
        assertEquals("", message.message)

        assertFalse(message.isValid())
    }

    @Test
    fun fileMessageFilled() {
        val message = MessageParser("/file talk.pem").parsedMessage()

        assertEquals(MessageKind.FILE, message.kind)
        assertEquals("SERVER", message.recipient)
        assertEquals("talk.pem", message.message)

        assertTrue(message.isValid())
    }

    @Test
    fun fileMessageMentioning() {
        val message = MessageParser("/file @emaiax talk.pem").parsedMessage()

        assertEquals(MessageKind.FILE, message.kind)
        assertEquals("@emaiax", message.recipient)
        assertEquals("talk.pem", message.message)

        assertTrue(message.isValid())
    }

    @Test
    fun publicMessageEmpty() {
        val message = MessageParser("    ").parsedMessage()

        assertEquals(MessageKind.PUBLIC_MESSAGE, message.kind)
        assertEquals("SERVER", message.recipient)
        assertEquals("", message.message)

        assertFalse(message.isValid())
    }

    @Test
    fun publicMessageFilled() {
        val message = MessageParser(" wazza ").parsedMessage()

        assertEquals(MessageKind.PUBLIC_MESSAGE, message.kind)
        assertEquals("SERVER", message.recipient)
        assertEquals("wazza", message.message)

        assertTrue(message.isValid())
    }

    @Test
    fun publicMessageMentioning() {
        val message = MessageParser("@emaiax hey").parsedMessage()

        assertEquals(MessageKind.PUBLIC_MESSAGE, message.kind)
        assertEquals("SERVER", message.recipient)
        assertEquals("@emaiax hey", message.message)

        assertTrue(message.isValid())
    }
}