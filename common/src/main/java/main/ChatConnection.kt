package main

import java.io.*
import java.net.Socket
import kotlin.concurrent.thread

open class ChatConnection(private val chat: Chat, protected val connection: Socket) {
    private lateinit var outputStream: ObjectOutputStream
    private lateinit var inputStream: ObjectInputStream

    open fun start() {
        // ObjectOutputStream must be created before ObjectInputStream
        // https://stackoverflow.com/questions/14110986/new-objectinputstream-blocks
        //
        outputStream = ObjectOutputStream(connection.getOutputStream())
        inputStream = ObjectInputStream(connection.getInputStream())

        receive()
    }

    open fun close() {
        inputStream.close()
        outputStream.close()
        connection.close()

        println("Connection disconnected.")
    }

    private fun receive() {
        thread(true) {
            try {
                inputStream.readObject().let {
                    chat.handleMessages(this, it as Message)

                    receive()
                }
            } catch (ioe: IOException) {
                println("Receiving IO error: ${ioe.message}")

                chat.disconnect(this)
            } catch (e: Exception) {
                println("Generic error on RECEIVE: ${e.message}")
            }
        }
    }

    fun send(message: Message) {
        try {
            outputStream.let { it.writeObject(message) }
        } catch (ioe: IOException) {
            println("Sending IO error: ${ioe.message}")

            chat.disconnect(this)
        } catch (e: Exception) {
            println("Generic error on SEND: ${e.message}")
        }
    }
}