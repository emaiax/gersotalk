package main

import java.io.File
import java.io.Serializable
import java.time.LocalDateTime

data class Message(val kind: MessageKind, val source: String, val recipient: String, val message: String) : Serializable {
    val file: ByteArray?

    init {
        file = when (kind) {
            MessageKind.FILE -> {
                val f = File(message)

                if (f.exists()) f.readBytes() else null
            }
            else -> null
        }
    }

    fun text() : String {
        return "[${LocalDateTime.now()}] $source: $message"
    }

    fun isValid() : Boolean {
        return when(kind) {
            MessageKind.QUIT -> true
            else -> {
                recipient.isNotBlank() && message.isNotBlank()
            }
        }
    }
}

class MessageParser(private val text: String, origin: String?) {
    private var kind: MessageKind
    private var source: String
    private var recipient: String
    private var statement: String

    init {
        val inputs = text.split("\\s".toRegex())

        kind      = getKind(inputs.first())
        statement = getStatement(kind)
        recipient = getRecipient(kind, inputs) ?: "SERVER"
        source    = origin ?: getSource(kind, inputs)
    }

    fun parsedMessage() : Message {
        val parsed = text.let {
            it.removePrefix(statement)
                .trim()
                .removePrefix(recipient)
                .trim()
        }

        return Message(kind, source, recipient, parsed)
    }

    private fun getSource(kind: MessageKind, inputs: List<String>) : String {
        return if (kind == MessageKind.IDENTIFY && inputs.size > 1) {
            "@${inputs[1].removePrefix("@")}"
        } else {
            "UNKNOWN"
        }
    }

    private fun getKind(input: String): MessageKind {
        return when (input.toLowerCase()) {
            "/file" -> MessageKind.FILE
            "/quit" -> MessageKind.QUIT
            "/pvt" -> MessageKind.PRIVATE_MESSAGE
            "/identify" -> MessageKind.IDENTIFY
            else -> MessageKind.PUBLIC_MESSAGE
        }
    }

    private fun getStatement(kind: MessageKind) : String {
        return when (kind) {
            MessageKind.PUBLIC_MESSAGE -> "/text"
            MessageKind.PRIVATE_MESSAGE -> "/pvt"
            MessageKind.IDENTIFY -> "/identify"
            MessageKind.FILE -> "/file"
            MessageKind.QUIT -> "/quit"
        }
    }

    private fun getRecipient(kind: MessageKind, inputs: List<String>): String? {
        return when (kind) {
            MessageKind.FILE, MessageKind.PRIVATE_MESSAGE -> {
                if (inputs.size == 1) {
                    null
                } else {
                    if (inputs[1].startsWith("@")) {
                        inputs[1]
                    } else {
                        null
                    }
                }
            }
            else -> null
        }
    }
}
