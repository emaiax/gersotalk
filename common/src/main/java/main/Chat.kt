package main

import java.util.UUID
import java.net.Socket

open class Chat {
    var chatId = UUID.randomUUID().toString()

    open fun start() {}
    open fun connect(socket: Socket) {}
    open fun disconnect(connection: ChatConnection?) {}

    open fun listenForBroadcast() {}
    open fun broadcast(text: String?) {}

    open fun handleMessages(connection: ChatConnection?, message: Message) {}
}