package main

enum class MessageKind {
    FILE,
    QUIT,
    IDENTIFY,
    PUBLIC_MESSAGE,
    PRIVATE_MESSAGE
}