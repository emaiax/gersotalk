package main

import java.io.File

object FileUtils {
    fun prepareFolders(baseName: String) {
        val basePaths = "./$baseName/files/"

        listOf("received", "sent").forEach { folder ->
            val dir = File(basePaths + folder)

            if (!dir.exists()) { dir.mkdirs() }
        }
    }

    fun saveSentFile(baseName: String, filePath: String, subDir: String?) : String {
        val originalFile = File(filePath)

        val destinationFilePath = folderName("./$baseName/files/sent", subDir).let {
            if (!File(it).exists()) { File(it).mkdirs() }

            "$it/${originalFile.name}"
        }

        originalFile.copyTo(File(destinationFilePath), overwrite = true)

        return destinationFilePath
    }

    fun saveReceivedFile(baseName: String, filePath: String, content: ByteArray, subDir: String?) : String {
        val originalFile = File(filePath)

        val destinationFilePath = folderName("./$baseName/files/received", subDir).let {
            if (!File(it).exists()) { File(it).mkdirs() }

            "$it/${originalFile.name}"
        }

        File(destinationFilePath).writeBytes(content)

        return destinationFilePath
    }

    private fun folderName(baseName: String, subDir: String?) : String {
        return if (subDir.isNullOrBlank()) baseName else "$baseName/${subDir.removePrefix("@")}"
    }
}