package main

import java.net.*

class ChatClientConnection(chat: Chat, connection: Socket) : ChatConnection(chat, connection) {
    override fun start() {
        println("Connected to $connection")

        super.start()
    }
}