package main;

object Client {
    @JvmStatic
    fun main(args: Array<String>) {
        FileUtils.prepareFolders("client")

        ChatClient("127.0.0.1", 5555).start()
    }
}