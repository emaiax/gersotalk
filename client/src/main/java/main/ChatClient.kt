package main

import java.io.*
import java.net.*
import kotlin.concurrent.thread

class ChatClient(private val server: String, private val port: Int) : Chat() {
    private lateinit var connection: ChatClientConnection

    override fun start() {
        try {
            println("Establishing connection. Please wait...")

            if (handshake()) {
                listenForBroadcast()
            } else {
                println("Identification incomplete. Good bye.")
            }
        } catch (uhe: UnknownHostException) {
            println("Host unknown: ${uhe.message}")
        } catch (ioe: IOException) {
            println("Unexpected exception: " + ioe.message)
        }
    }

    fun handshake() : Boolean {
        var identified = false
        var identification = ""

        println(">> You're connected. Identify yourself. [/identify @your_nickname]")

        while (identification.isNullOrBlank() || !identified) {
            identification = readLine()!!

            val message = MessageParser(identification, null).parsedMessage()

            identified = when (message.kind) {
                MessageKind.IDENTIFY -> {
                    chatId = message.source

                    connect(Socket(server, port))

                    broadcast(identification)

                    println(">> You joined the room.")

                    return true
                }
                MessageKind.QUIT -> return false // immediately quits
                else -> {
                    println(">> Invalid identification. Try again or quit. [/identify @your_nickname]")

                    false
                }
            }
        }

        return false
    }

    override fun connect(socket: Socket) {
        connection = ChatClientConnection(this, socket).apply {
            this.start()
        }
    }

    override fun listenForBroadcast() {
        thread(true) {
            broadcast(readLine())
            listenForBroadcast()
        }
    }

    override fun broadcast(text: String?) {
        val message = MessageParser(text!!, chatId).parsedMessage()

        if (message.kind == MessageKind.FILE) {
            if (message.file != null) {
                FileUtils.saveSentFile("client", message.message, message.source)

                println("** [SENDING FILE] ${message.message} to ${message.recipient}")
            } else {
                println(">> File ${message.message} does not exists. Please, use absolute paths. Skipping transfer.")
            }

        }

        connection.send(message)
    }

    override fun handleMessages(connection: ChatConnection?, message: Message) {
        synchronized(this) {
            when (message.kind) {
                MessageKind.QUIT -> {
                    if (chatId == message.source) {
                        disconnect(connection)

                        println(">> Good bye. Press RETURN to exit ...")
                    } else {
                        println(">> ${message.text()}")
                    }
                }
                MessageKind.PRIVATE_MESSAGE -> {
                    println("** [PRIVATE TO ${message.recipient}] ${message.text()}")
                }

                MessageKind.FILE -> {
                    if (chatId == message.recipient) {
                        val copyFilePath = FileUtils.saveReceivedFile("client", message.message, message.file!!, message.recipient)

                        println("** [RECEIVING FILE] $copyFilePath from ${message.source}")
                    }
                }
                else -> println(">> ${message.text()}")
            }
        }

    }

    override fun disconnect(connection: ChatConnection?) {
        synchronized(this) {
            connection.apply {
                this?.close()
            }
        }
    }
}