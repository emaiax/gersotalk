package main

import java.net.*
import kotlin.concurrent.thread

class ChatServer(private val port: Int) : Chat() {
    private lateinit var server: ServerSocket
    private val connections = ArrayList<ChatConnection>()

    override fun start() {
        println("Binding server to port $port, please wait...")
        server = ServerSocket(port)
        println("Server started: $server, listening for incoming connections...")

        listenForClients()
        listenForBroadcast()
    }

    private fun listenForClients() {
        thread(true) {
            connect(server.accept())
            listenForClients()
        }
    }

    override fun listenForBroadcast() {
        thread(true) {
            broadcast(readLine())
            listenForBroadcast()
        }
    }

    override fun connect(socket: Socket) {
        thread {
            ChatServerConnection(this, socket).apply {
                this.start()

                connections.add(this)
            }
        }
    }

    override fun broadcast(text: String?) {
        val message = MessageParser(text!!, "SERVER").parsedMessage()

        println(">> ${text}")

        connections.forEach { conn -> conn.send(message) }
    }

    override fun handleMessages(connection: ChatConnection?, message: Message) {
        synchronized(this) {
            val serverConnection = connection as ChatServerConnection

            when (message.kind) {
                MessageKind.IDENTIFY -> {
                    serverConnection.clientId = message.source

                    broadcast("${message.source} joined the room.")
                }

                MessageKind.PUBLIC_MESSAGE -> {
                    println(message.text())

                    connections.forEach { conn -> conn.send(message) }
                }

                MessageKind.PRIVATE_MESSAGE -> {
                    if (message.recipient == "SERVER") {
                        println("** [PRIVATE] ${message.text()}")
                    } else {
                        val peopleInvolved = listOf(message.recipient, message.source)

                        connections.filter { c -> (c as ChatServerConnection).clientId in peopleInvolved }.forEach { conn -> conn.send(message) }
                    }
                }

                MessageKind.QUIT -> {
                    broadcast("${message.source} left the room. (/quit ${message.message})")

                    disconnect(connection)
                }

                MessageKind.FILE -> {
                    if (message.recipient == "SERVER") {
                        val copyFilePath = FileUtils.saveReceivedFile("server", message.message, message.file!!, message.recipient)

                        println("** [PRIVATE FILE] ${message.message} from ${message.source}. Copy saved to $copyFilePath")
                    } else {
                        val peopleInvolved = listOf(message.recipient, message.source)

                        connections.filter { c -> (c as ChatServerConnection).clientId in peopleInvolved }.forEach { conn -> conn.send(message) }
                    }
                }

                else -> {
                    println("not yet: ${message.text()}")
                }
            }
        }
    }

    override fun disconnect(connection: ChatConnection?) {
        synchronized(this) {
            connection.apply {
                this?.close()

                connections.remove(this)
            }
        }
    }
}