package main

object Server {
    @JvmStatic
    fun main(args: Array<String>) {
        FileUtils.prepareFolders("server")

        ChatServer(5555).start()
    }
}